﻿using System.Collections.Generic;

namespace Coursebase.model
{
    public class Keyword
    {
        public int Id { get; set; }
        public string Word { get; set; }

        public virtual ICollection<KeywordsForCourses> KeywordsForCourses { get; set; }
    }
}
