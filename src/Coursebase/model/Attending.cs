﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coursebase.model
{
    public class Attending
    {
        public int CourseId { get; set; }
        public Course Course { get; set; }

        public int StudentId { get; set; }
        public Student Student { get; set; }

        public int CourseInstanceId { get; set; }
        public CourseInstance CourseInstance { get; set; }
    }
}
