﻿using System.Collections.Generic;

namespace Coursebase.model
{
    public class Cohort
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Student> Students { get; set; }
    }
}
