﻿using System.Collections.Generic;

namespace Coursebase.model
{
    public class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

        public int? CohortId { get; set; }
        public Cohort Cohort { get; set; }
        public virtual ICollection<Attending> Attendings { get; set; }
    }
}
