﻿using System.Collections.Generic;

namespace Coursebase.model
{
    public class Course
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public virtual ICollection<CourseInstance> CourseInstances { get; set; }
        public virtual ICollection<Attending> Attendings { get; set; }
        public virtual ICollection<KeywordsForCourses> KeywordsForCourses { get; set; }
    }
}
