﻿using System;
using System.Collections.Generic;

namespace Coursebase.model
{
    public class CourseInstance
    {
        public int Id { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public int Seats { get; set; }

        public int? CourseId { get; set; }
        public Course Course { get; set; }
        public virtual ICollection<Attending> Attendings { get; set; }
    }
}
