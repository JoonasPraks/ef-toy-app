using Coursebase.model;

namespace Coursebase
{
    interface ICohortRepository : IRepository<Cohort> {
        bool AddStudentToCohort(Cohort cohort, Student student);
        bool RemoveStudentFromCohort(Cohort cohort, Student student);
        bool RegisterCohordForCourse(CourseInstance instance, Cohort cohort);
    }
}
