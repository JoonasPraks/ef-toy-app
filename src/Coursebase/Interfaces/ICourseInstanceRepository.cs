using System;
using System.Collections.Generic;
using Coursebase.model;

namespace Coursebase
{
    interface ICourseInstanceRepository : IRepository<CourseInstance> {
        //Changed T from Course to CourseInstance
        IEnumerable<CourseInstance> Read(Course course, DateTime? start = null, DateTime? end = null);
        bool RegisterStudentForCourse(CourseInstance instance, Student student);
        bool DeregisterStudentFromCourse(CourseInstance instance, Student student);
    }
}
