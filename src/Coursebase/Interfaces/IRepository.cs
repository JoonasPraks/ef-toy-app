using System;
using System.Collections.Generic;

namespace Coursebase
{
    interface IRepository<T> : IDisposable {
        IEnumerable<T> Read();
        T Read(int id);
        int Create(T entity);
        int Update(T entity);
        int Delete(T entity);
    }
}

