using System.Collections.Generic;
using Coursebase.model;

namespace Coursebase
{
    interface IStudentRepository : IRepository<Student> {
        IEnumerable<Student> Read(string keyword);
    }
}
