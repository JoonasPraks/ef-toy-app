using Coursebase.model;

namespace Coursebase
{
    interface IKeywordRepository : IRepository<Keyword> {
        bool AddKeywordToCourse(Course course, Keyword keyword);
        bool RemoveKeywordFromCourse(Course course, Keyword keyword);
    }
}
