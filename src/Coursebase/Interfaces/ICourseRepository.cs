using System.Collections.Generic;
using Coursebase.model;

namespace Coursebase
{
    interface ICourseRepository : IRepository<Course> {
        IEnumerable<Course> Read(Keyword keyword);
        IEnumerable<Course> GetCoursesForStudent(Student student);
    }
}
