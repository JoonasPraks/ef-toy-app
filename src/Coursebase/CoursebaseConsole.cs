using System;
using Coursebase.model;
using System.Linq;

namespace Coursebase
{
    public class CoursebaseConsole
    {
        public static void Main()
        {
            CoursebaseContext context = new CoursebaseContext();
            ICourseRepository courseRepo = new CourseRepository(context);
            IStudentRepository studentRepo = new StudentRepository(context);

            {
                var student = new Student { Name = "Jacob", Email = "jbec@itu.dk" };
                studentRepo.Create(student);
            }

            Console.WriteLine("Hi and welcome to the Coursebase Console!");
            Console.Write("> ");

            string input;
            while ((input = Console.ReadLine()) != "exit")
            {
                if (input == "courses")
                {
                    Console.WriteLine("The courses available is:");
                    foreach (var course in courseRepo.Read())
                    {
                        Console.WriteLine($"\t{course.Title}");
                    }
                }
                else
                {
                    Console.WriteLine($"The command '{input}' is not known");
                }
                Console.Write("> ");
            }

            /*CoursebaseContext context = new CoursebaseContext();
           StudentRepository studentRepo = new StudentRepository(context);
           CohortRepository cohortRepo = new CohortRepository(context);
           CourseRepository courseRepo = new CourseRepository(context);
           CourseInstanceRepository courseInstanceRepo = new CourseInstanceRepository(context);

           var course = new Course { Description = "bio", Title = "Biology" };
           context.Courses.Add(course);


           /*var cohort = new Cohort { Name = "Group2" };
           var student = new Student { Name = "Kaarel", Email = "joonaspraks" };

           Console.WriteLine(cohortRepo.AddStudentToCohort(cohort, student));*/


            /*foreach (var student in studentRepo.Read(Console.ReadLine()))
            {
                Console.WriteLine($"{student.Name}, { student.Email}");
            }*/

            /*var student1 = new Student { Name = "Joonas", Email = "joonaspraks" };
            var cohort = new Cohort { Name = "Group2"};
            context.Students.Add(student1);
            context.Cohort.Add(cohort);
            context.SaveChanges();
            Console.WriteLine("Done");*/

            /*var cohort = from s in context.Students
                         select new {Name = s.Name, CohortName = s.Cohort.Name};

            foreach (var c in cohort)
            {
                Console.WriteLine(c);
            }*/

        }
    }
}

