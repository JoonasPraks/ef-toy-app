﻿using System;
using System.Collections.Generic;
using Coursebase.model;
using System.Linq;

namespace Coursebase
{
    internal class CourseRepository : ICourseRepository
    {
        private readonly CoursebaseContext context;
        public CourseRepository(CoursebaseContext context)
        {
            this.context = context;
        }

        public int Create(Course entity)
        {
            context.Courses.Add(entity);
            int records = context.SaveChanges();

            return records;
        }

        public int Delete(Course entity)
        {
            var id = entity.Id;
            context.Courses.Remove(entity);
            context.SaveChanges();

            return id;
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public IEnumerable<Course> GetCoursesForStudent(Student student)
        {                   
            return from ats in student.Attendings
                                select ats.Course;
        }

        public IEnumerable<Course> Read()
        {
            return context.Courses;
        }

        public Course Read(int id)
        {
            var course = from c in context.Courses
                          where c.Id == id
                          select c;

            return course.FirstOrDefault();
        }

        public IEnumerable<Course> Read(Keyword keyword)
        {
            return from k in keyword.KeywordsForCourses
                          select k.Course;
        }

        public int Update(Course entity)
        {
            context.Courses.Update(entity);
            context.SaveChanges();

            return entity.Id;
        }
    }
}