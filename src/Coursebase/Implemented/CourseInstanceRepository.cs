﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Coursebase.model;

namespace Coursebase
{
    public class CourseInstanceRepository : ICourseInstanceRepository
    {
        private readonly CoursebaseContext context;
        public CourseInstanceRepository(CoursebaseContext context)
        {
            this.context = context;
        }

        public int Create(CourseInstance entity)
        {
            context.CourseInstances.Add(entity);
            int records = context.SaveChanges();

            return records;
        }

        public int Delete(CourseInstance entity)
        {
            var id = entity.Id;
            context.CourseInstances.Remove(entity);
            context.SaveChanges();

            return id;
        }
        public bool DeregisterStudentFromCourse(CourseInstance instance, Student student)
        {
            var attendRem = instance.Attendings
                .Where(a => a.Student == student)
                .FirstOrDefault();

            instance.Attendings.Remove(attendRem);
            context.SaveChanges();
            
            return !instance.Attendings.Contains(attendRem);
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public IEnumerable<CourseInstance> Read()
        {
            return context.CourseInstances;
        }

        public CourseInstance Read(int id)
        {
            var course = from c in context.CourseInstances
                         where c.Id == id
                         select c;

            return course.FirstOrDefault();
        }

        public IEnumerable<CourseInstance> Read(Course course, DateTime? start = default(DateTime?), DateTime? end = default(DateTime?))
        {
            return context.CourseInstances.Where(cI => cI.Course == course && cI.Start == start && cI.End == end);
        }

        public bool RegisterStudentForCourse(CourseInstance instance, Student student)
        {
            var newAtt = new Attending { Course = instance.Course, CourseInstance = instance, Student = student };
            instance.Attendings.Add(newAtt);
            context.SaveChanges();

            return instance.Attendings.Contains(newAtt);
        }

        public int Update(CourseInstance entity)
        {
            context.CourseInstances.Update(entity);
            context.SaveChanges();

            return entity.Id;
        }
    }
}
