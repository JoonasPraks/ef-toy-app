﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Coursebase.model;

namespace Coursebase
{
    public class CohortRepository : ICohortRepository
    {
        private readonly CoursebaseContext context;
        public CohortRepository(CoursebaseContext context)
        {
            this.context = context;
        }

        public bool AddStudentToCohort(Cohort cohort, Student student)
        {
            var cId = context.Cohort
                .Where(x => x.Name == cohort.Name)
                .FirstOrDefault().Id;

            var cStudent = context.Students
                .Where(x => x.Name == student.Name && x.Email == student.Email)
                .FirstOrDefault();

            cStudent.CohortId = cId;
            context.SaveChanges();

            return cStudent.CohortId == cId;
        }

        public int Create(Cohort entity)
        {
            context.Cohort.Add(entity);
            int records = context.SaveChanges();

            return records;
        }

        public int Delete(Cohort entity)
        {
            var id = entity.Id;
            context.Cohort.Remove(entity);
            context.SaveChanges();

            return id;
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public IEnumerable<Cohort> Read()
        {
            return context.Cohort;
        }

        public Cohort Read(int id)
        {
            var course = from c in context.Cohort
                         where c.Id == id
                         select c;

            return course.FirstOrDefault();
        }

        public bool RegisterCohordForCourse(CourseInstance instance, Cohort cohort)
        {
            //Cohort consists of students so just add new attending for student in cohort
            foreach (var student in cohort.Students)
            {
                student.Attendings.Add(new Attending
                {
                    Course = instance.Course,
                    Student = student,
                    CourseInstance = instance
                });
            }
            int sc = context.SaveChanges();

            return cohort.Students.Count() == sc;
        }

        public bool RemoveStudentFromCohort(Cohort cohort, Student student)
        {
            var cId = context.Cohort
                .Where(x => x.Name == cohort.Name)
                .FirstOrDefault().Id;

            var cStudentRem = context.Students
                .Where(x => x.Name == student.Name && x.Email == student.Email)
                .FirstOrDefault();

            var cStudent = cStudentRem.CohortId;

            if (cStudent == cId)
            {
                cStudentRem.CohortId = null;
            }
            context.SaveChanges();

            return cStudentRem.CohortId == null;
        }

        public int Update(Cohort entity)
        {
            context.Cohort.Update(entity);
            context.SaveChanges();

            return entity.Id;
        }
    }
}
