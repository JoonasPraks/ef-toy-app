﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Coursebase.model;

namespace Coursebase
{
    public class StudentRepository : IStudentRepository
    {
        private readonly CoursebaseContext context;
        public StudentRepository (CoursebaseContext context)
        {
            this.context = context;
        }

        public int Create(Student entity)
        {
            context.Students.Add(entity);
            int records = context.SaveChanges();

            return records;
        }

        public int Delete(Student entity)
        {
            var id = entity.Id;                
            context.Students.Remove(entity);
            context.SaveChanges();
                
            return id;          
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public IEnumerable<Student> Read()
        {
            return context.Students;               
        }

        public Student Read(int id)
        {
            var student = from s in context.Students
                          where s.Id == id
                          select s;

            return student.FirstOrDefault();
        }

        public IEnumerable<Student> Read(string keyword)
        {
            var students = from s in context.Students
                           where (s.Name.Contains(keyword) || s.Email.Contains(keyword))
                           select s;

            return students;           
        }

        public int Update(Student entity)
        {

            context.Students.Update(entity);
            context.SaveChanges();

            return entity.Id;
        }
    }
}
