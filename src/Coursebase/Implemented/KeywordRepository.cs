﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Coursebase.model;

namespace Coursebase.Implemented
{
    public class KeywordRepository : IKeywordRepository
    {
        private readonly CoursebaseContext context;
        public KeywordRepository(CoursebaseContext context)
        {
            this.context = context;
        }

        public bool AddKeywordToCourse(Course course, Keyword keyword)
        {
            var newKForC = new KeywordsForCourses { Keyword = keyword, Course = course };
            keyword.KeywordsForCourses.Add(newKForC);
            context.SaveChanges();

            return course.KeywordsForCourses.Contains(newKForC);
        }

        public int Create(Keyword entity)
        {
            context.Keywords.Add(entity);
            int records = context.SaveChanges();

            return records;
        }

        public int Delete(Keyword entity)
        {
            var id = entity.Id;
            context.Keywords.Remove(entity);
            context.SaveChanges();

            return id;
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public IEnumerable<Keyword> Read()
        {
            return context.Keywords;
        }

        public Keyword Read(int id)
        {
            var keyWord = from k in context.Keywords
                         where k.Id == id
                         select k;

            return keyWord.FirstOrDefault();
        }

        public bool RemoveKeywordFromCourse(Course course, Keyword keyword)
        {
            var keyWordRem = keyword.KeywordsForCourses.Where(k => k.Keyword == keyword && k.Course == course).FirstOrDefault();
            course.KeywordsForCourses.Remove(keyWordRem);

            return !course.KeywordsForCourses.Contains(keyWordRem);
        }

        public int Update(Keyword entity)
        {
            context.Keywords.Update(entity);
            context.SaveChanges();

            return entity.Id;
        }
    }
}
