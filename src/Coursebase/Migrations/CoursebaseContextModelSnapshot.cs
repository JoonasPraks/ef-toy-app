﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Coursebase;

namespace Coursebase.Migrations
{
    [DbContext(typeof(CoursebaseContext))]
    partial class CoursebaseContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.1");

            modelBuilder.Entity("Coursebase.model.Attending", b =>
                {
                    b.Property<int>("CourseId");

                    b.Property<int>("StudentId");

                    b.Property<int>("CourseInstanceId");

                    b.HasKey("CourseId", "StudentId", "CourseInstanceId");

                    b.HasIndex("CourseId");

                    b.HasIndex("CourseInstanceId");

                    b.HasIndex("StudentId");

                    b.ToTable("Attending");
                });

            modelBuilder.Entity("Coursebase.model.Cohort", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Cohort");
                });

            modelBuilder.Entity("Coursebase.model.Course", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.Property<string>("Title");

                    b.HasKey("Id");

                    b.ToTable("Courses");
                });

            modelBuilder.Entity("Coursebase.model.CourseInstance", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("CourseId");

                    b.Property<DateTime>("End");

                    b.Property<int>("Seats");

                    b.Property<DateTime>("Start");

                    b.HasKey("Id");

                    b.HasIndex("CourseId");

                    b.ToTable("CourseInstances");
                });

            modelBuilder.Entity("Coursebase.model.Keyword", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Word");

                    b.HasKey("Id");

                    b.ToTable("Keywords");
                });

            modelBuilder.Entity("Coursebase.model.KeywordsForCourses", b =>
                {
                    b.Property<int>("CourseId");

                    b.Property<int>("KeywordId");

                    b.HasKey("CourseId", "KeywordId");

                    b.HasIndex("CourseId");

                    b.HasIndex("KeywordId");

                    b.ToTable("KeywordsForCourses");
                });

            modelBuilder.Entity("Coursebase.model.Student", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("CohortId");

                    b.Property<string>("Email");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasIndex("CohortId");

                    b.ToTable("Students");
                });

            modelBuilder.Entity("Coursebase.model.Attending", b =>
                {
                    b.HasOne("Coursebase.model.Course", "Course")
                        .WithMany("Attendings")
                        .HasForeignKey("CourseId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Coursebase.model.CourseInstance", "CourseInstance")
                        .WithMany("Attendings")
                        .HasForeignKey("CourseInstanceId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Coursebase.model.Student", "Student")
                        .WithMany("Attendings")
                        .HasForeignKey("StudentId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Coursebase.model.CourseInstance", b =>
                {
                    b.HasOne("Coursebase.model.Course", "Course")
                        .WithMany("CourseInstances")
                        .HasForeignKey("CourseId");
                });

            modelBuilder.Entity("Coursebase.model.KeywordsForCourses", b =>
                {
                    b.HasOne("Coursebase.model.Course", "Course")
                        .WithMany("KeywordsForCourses")
                        .HasForeignKey("CourseId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Coursebase.model.Keyword", "Keyword")
                        .WithMany("KeywordsForCourses")
                        .HasForeignKey("KeywordId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Coursebase.model.Student", b =>
                {
                    b.HasOne("Coursebase.model.Cohort", "Cohort")
                        .WithMany("Students")
                        .HasForeignKey("CohortId");
                });
        }
    }
}
