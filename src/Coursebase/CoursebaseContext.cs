﻿using System;
using System.Collections.Generic;
using System.IO;
using Coursebase.model;
using Microsoft.EntityFrameworkCore;

namespace Coursebase
{
	public class CoursebaseContext : DbContext
	{
		public DbSet<Student> Students { get; set; }
		public DbSet<Course> Courses { get; set; }
		public DbSet<CourseInstance> CourseInstances { get; set; }
		public DbSet<Keyword> Keywords { get; set; }
		public DbSet<Cohort> Cohort { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
            modelBuilder.Entity<Attending>().
                HasKey(x => new { x.CourseId, x.StudentId, x.CourseInstanceId });
            modelBuilder.Entity<KeywordsForCourses>().
                HasKey(x => new { x.CourseId, x.KeywordId });
        }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseSqlite("Filename=./coursebase.db");
		}
	}
}

