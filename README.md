Assignment #3: Week 38
======================
This week's indivudial assignment will be on UML, as well as using Entity Framework, Lambdas and LINQ in C#.

UML:
-------------------------------------
The object under study for these exercises will be the old coin-operated snack vending machines, placed throughout ITU. 
They require making a selection using a combination of a letter and a number in order to purchase an item. 
In case you are unfamiliar with these types of machines, you can set out to investigate how they respond to different input, 
or you can initiate a discussion on the forum/Slack.

- Create an activity diagram representing a purchase of an item, taking into account sufficient and insufficient funds being inserted, 
as well as cancellation of an order.
- Create a state diagram representing the object responsible for managing a transaction. A transaction involves the purchase of one item.

C# part:
----------------
In this C# assignment you are to implement and test a Data Access Layer for the University Course Base system. 
You are given a Data model and a set of interfaces. 
Your job is to implement the layer using Entity Framework Core, LINQ, and Lambdas to support the methods in the given interfaces, 
**and of course test your solution**. 
General information about EFCore can be found [here](https://docs.efproject.net/en/latest/platforms/netcore/new-db-sqlite.html) 
and information about EFCore relations can be found [here](http://ef.readthedocs.io/en/latest/modeling/relationships.html). 
Lastly, you can read about tests in .NET Core [here](https://docs.microsoft.com/en-us/dotnet/articles/core/testing/unit-testing-with-dotnet-test).
![Data model](datamodel.png)

Submitting the assignment:
--------------------------
To submit the assignment, enter your answers into the answers.md file, complete the code and make a pull request. If you are in doubt of how to do this, then ask the TA's. Your pull request title should be your ITU initials and the assignment number. Eg 'jbec-bdsa-003'. You can use this [Markdown cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

This assignment will partly be corrected by automatically checking that your solution builds, and that your tests pass. The status of this will be visible in the pull request. Passing all tests does not mean that you passed the assignment, and not passing all tests does not mean you didn't. You might have a good reason for having tests that fail.

The submission deadline is **25th of September (Sunday) at midnight** and the latest commit on your fork will be corrected. If you solution is not approved you can commit new changes and comment on the pull request that you have re-submitted. Assignments that is resubmitted, or submitted late, will be put last in our correction stack.
